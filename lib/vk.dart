/// A cross-platform client library for VK's public API.
library vk;

export 'src/api/api.dart';
export 'src/models/models.dart';
export 'src/exceptions/exceptions.dart';
export 'src/responses/responses.dart';
export 'src/api.dart';
export 'src/scopes.dart';