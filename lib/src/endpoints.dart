/// The URL to access to receive an VK authorization token.
final String authEndpoint = 'https://oauth.vk.com/';

final String apiEndpoint = 'https://api.vk.com/method/';