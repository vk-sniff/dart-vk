import 'dart:async';
import 'package:dart_vk/vk.dart';
import 'package:meta/meta.dart';

import 'client.dart';
import 'models/models.dart';

class VkApi {
  final VkApiClient _client = VkApiClient();
  String token;

  PhotosApi get photos => PhotosApi(_client, token);
  FriendsApi get friends => FriendsApi(_client, token);
  UsersApi get users => UsersApi(_client, token);

  VkApi({
    this.token
  });

  Future<AuthInfo> login({
    @required String username,
    @required String password,
    String tfa,
    String captchaSid,
    String captchaKey,
    String clientId,
    String clientSecret,
    String grantType
  }) async {
    final client = VkAuthClient();
    final response = await client.get('token',
      queryParameters: {
        'client_id': clientId,
        'client_secret': clientSecret,
        'grant_type': grantType,
        'scope': 'Call',
        '2fa_supported': '1',
        'lang': 'en',
        'username': username,
        'password': password,
        if(tfa != null)
        'code': tfa,
        if(captchaSid != null && captchaKey != null)
          ...{
            'captcha_sid': captchaSid,
            'captcha_key': captchaKey
          }
      },
    );
    final json = response.data as Map<String, dynamic>;
    final user = AuthInfo.fromMap(json);
    this.token = user.accessToken;
    return user;
  }
}