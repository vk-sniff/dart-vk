
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:dart_vk/src/models/auth_info.dart';
import 'package:built_collection/built_collection.dart';

// Bug in built
import 'photo.dart';

import 'models.dart';
import 'serializers_custom.dart';

part 'serializers.g.dart';

@SerializersFor([
  VkPhotoSize,
  VkPhoto,
  AuthInfo,
  VkUser
])
final Serializers serializers = _$serializers;
final Serializers standardSerializers = (
  serializers
    .toBuilder()
    ..addPlugin(StandardJsonPlugin())
    ..add(BoolAsIntSerializer())
).build();