import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'vk_user.g.dart';

abstract class LastSeen implements Built<LastSeen, LastSeenBuilder> {
  DateTime get time;
  int get platform;

  static Serializer<LastSeen> get serializer => _$lastSeenSerializer;
  LastSeen._();
  factory LastSeen([updates(LastSeenBuilder b)]) = _$LastSeen;
  factory LastSeen.fromMap(Map<String, dynamic> map) => standardSerializers.deserializeWith(serializer, map);
  Map<String, dynamic> toMap() => standardSerializers.serializeWith(serializer, this);
}


abstract class VkUser implements Built<VkUser, VkUserBuilder> {
  int get id;

  // Required parameters
  @BuiltValueField(wireName: 'first_name')
  String get firstName;
  @BuiltValueField(wireName: 'last_name')
  String get lastName;
  @nullable String get deactivated;
  @BuiltValueField(wireName: 'is_closed')
  bool get isClosed;
  @BuiltValueField(wireName: 'can_access_closed')
  bool get canAccessClosed;

  // Optional parameters
  @nullable String get about;
  @nullable String get activities;
  @nullable String get bdate;
  @nullable bool get blacklisted;
  @BuiltValueField(wireName: 'blacklisted_by_me')
  @nullable bool get blacklistedByMe;
  @nullable String get books;
  @BuiltValueField(wireName: 'can_post')
  @nullable bool get canPost;
  @BuiltValueField(wireName: 'can_see_all_posts')
  @nullable bool get canSeeAllPosts;
  @BuiltValueField(wireName: 'can_see_audio')
  @nullable bool get canSeeAudio;
  @BuiltValueField(wireName: 'can_send_friend_request')
  @nullable bool get canSendFriendRequest;
  @BuiltValueField(wireName: 'can_write_private_message')
  @nullable bool get canWritePrivateMessage;
  // NotImplemented get career;
  // NotImplemented get city;
  @BuiltValueField(wireName: 'common_count')
  @nullable int get commonCount;
  @nullable Map<String, String> get connections;
  // NotImplemented get contacts;
  // NotImplemented get counters;
  // NotImplemented get country;
  // NotImplemented get crop_photo;
  @nullable String get domain;
  // NotImplemented get education;
  // NotImplemented get exports;
  @BuiltValueField(wireName: 'followers_count')
  @nullable int get followersCount;
  @BuiltValueField(wireName: 'friend_status')
  @nullable int get friendStatus;
  @nullable String get games;
  @BuiltValueField(wireName: 'has_mobile')
  @nullable bool get hasMobile;
  @BuiltValueField(wireName: 'has_photo')
  @nullable bool get hasPhoto;
  @BuiltValueField(wireName: 'home_town')
  @nullable String get homeTown;
  @nullable String get interests;
  @BuiltValueField(wireName: 'is_favorite')
  @nullable bool get isFavorite;
  @BuiltValueField(wireName: 'is_friend')
  @nullable bool get isFriend;
  @BuiltValueField(wireName: 'is_hidden_from_feed')
  @nullable bool get isHiddenFromFeed;
  @BuiltValueField(wireName: 'last_seen')
  @nullable LastSeen get lastSeen;
  @nullable BuiltList<int> get lists;
  @BuiltValueField(wireName: 'maiden_name')
  @nullable String get maidenName;
  // NotImplemented get military;
  @nullable String get movies;
  @nullable String get music;
  @nullable String get nickname;
  // NotImplemented get occupation;
  @nullable bool get online;
  // NotImplemented get personal;
  @BuiltValueField(wireName: 'photo_50')
  @nullable String get photo50;
  @BuiltValueField(wireName: 'photo_100')
  @nullable String get photo100;
  @BuiltValueField(wireName: 'photo_200_orig')
  @nullable String get photo200Orig;
  @BuiltValueField(wireName: 'photo_200')
  @nullable String get photo200;
  @BuiltValueField(wireName: 'photo_400_orig')
  @nullable String get photo400Orig;
  @BuiltValueField(wireName: 'photo_id')
  @nullable String get photoId;
  @BuiltValueField(wireName: 'photo_max')
  @nullable String get photoMax;
  @BuiltValueField(wireName: 'photo_max_orig')
  @nullable String get photoMaxOrig;
  @nullable String get quotes;
  // NotImplemented get relatives;
  @nullable int get relation;
  // NotImplemented get schools;
  @BuiltValueField(wireName: 'screen_name')
  @nullable String get screenName;
  @nullable int get sex;
  @nullable String get site;
  @nullable String get status;
  @nullable int get timezone;
  @nullable bool get trending;
  @nullable String get tv;
  // NotImplemented get universities;
  @nullable bool get verified;
  @BuiltValueField(wireName: 'wall_default')
  @nullable String get wallDefault;
  

  static Serializer<VkUser> get serializer => _$vkUserSerializer;
  VkUser._();
  factory VkUser([updates(VkUserBuilder b)]) = _$VkUser;
  factory VkUser.fromMap(Map<String, dynamic> map) => standardSerializers.deserializeWith(serializer, map);
  Map<String, dynamic> toMap() => standardSerializers.serializeWith(serializer, this);
}
