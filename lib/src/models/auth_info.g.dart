// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AuthInfo> _$authInfoSerializer = new _$AuthInfoSerializer();

class _$AuthInfoSerializer implements StructuredSerializer<AuthInfo> {
  @override
  final Iterable<Type> types = const [AuthInfo, _$AuthInfo];
  @override
  final String wireName = 'AuthInfo';

  @override
  Iterable<Object> serialize(Serializers serializers, AuthInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'access_token',
      serializers.serialize(object.accessToken,
          specifiedType: const FullType(String)),
      'user_id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  AuthInfo deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AuthInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'access_token':
          result.accessToken = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$AuthInfo extends AuthInfo {
  @override
  final String accessToken;
  @override
  final int id;

  factory _$AuthInfo([void Function(AuthInfoBuilder) updates]) =>
      (new AuthInfoBuilder()..update(updates)).build();

  _$AuthInfo._({this.accessToken, this.id}) : super._() {
    if (accessToken == null) {
      throw new BuiltValueNullFieldError('AuthInfo', 'accessToken');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('AuthInfo', 'id');
    }
  }

  @override
  AuthInfo rebuild(void Function(AuthInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthInfoBuilder toBuilder() => new AuthInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthInfo &&
        accessToken == other.accessToken &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, accessToken.hashCode), id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AuthInfo')
          ..add('accessToken', accessToken)
          ..add('id', id))
        .toString();
  }
}

class AuthInfoBuilder implements Builder<AuthInfo, AuthInfoBuilder> {
  _$AuthInfo _$v;

  String _accessToken;
  String get accessToken => _$this._accessToken;
  set accessToken(String accessToken) => _$this._accessToken = accessToken;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  AuthInfoBuilder();

  AuthInfoBuilder get _$this {
    if (_$v != null) {
      _accessToken = _$v.accessToken;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AuthInfo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AuthInfo;
  }

  @override
  void update(void Function(AuthInfoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AuthInfo build() {
    final _$result = _$v ?? new _$AuthInfo._(accessToken: accessToken, id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
