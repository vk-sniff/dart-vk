// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<VkPhotoSize> _$vkPhotoSizeSerializer = new _$VkPhotoSizeSerializer();
Serializer<VkPhoto> _$vkPhotoSerializer = new _$VkPhotoSerializer();

class _$VkPhotoSizeSerializer implements StructuredSerializer<VkPhotoSize> {
  @override
  final Iterable<Type> types = const [VkPhotoSize, _$VkPhotoSize];
  @override
  final String wireName = 'VkPhotoSize';

  @override
  Iterable<Object> serialize(Serializers serializers, VkPhotoSize object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'src',
      serializers.serialize(object.src, specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'width',
      serializers.serialize(object.width, specifiedType: const FullType(int)),
      'height',
      serializers.serialize(object.height, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  VkPhotoSize deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new VkPhotoSizeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'src':
          result.src = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'width':
          result.width = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'height':
          result.height = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$VkPhotoSerializer implements StructuredSerializer<VkPhoto> {
  @override
  final Iterable<Type> types = const [VkPhoto, _$VkPhoto];
  @override
  final String wireName = 'VkPhoto';

  @override
  Iterable<Object> serialize(Serializers serializers, VkPhoto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'owner_id',
      serializers.serialize(object.ownerId,
          specifiedType: const FullType(String)),
      'album_id',
      serializers.serialize(object.albumId,
          specifiedType: const FullType(String)),
      'user_id',
      serializers.serialize(object.userId,
          specifiedType: const FullType(String)),
      'text',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
      'date',
      serializers.serialize(object.date,
          specifiedType: const FullType(DateTime)),
      'sizes',
      serializers.serialize(object.sizes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(VkPhotoSize)])),
    ];

    return result;
  }

  @override
  VkPhoto deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new VkPhotoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'owner_id':
          result.ownerId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'album_id':
          result.albumId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user_id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'text':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'sizes':
          result.sizes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(VkPhotoSize)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$VkPhotoSize extends VkPhotoSize {
  @override
  final String src;
  @override
  final String type;
  @override
  final int width;
  @override
  final int height;

  factory _$VkPhotoSize([void Function(VkPhotoSizeBuilder) updates]) =>
      (new VkPhotoSizeBuilder()..update(updates)).build();

  _$VkPhotoSize._({this.src, this.type, this.width, this.height}) : super._() {
    if (src == null) {
      throw new BuiltValueNullFieldError('VkPhotoSize', 'src');
    }
    if (type == null) {
      throw new BuiltValueNullFieldError('VkPhotoSize', 'type');
    }
    if (width == null) {
      throw new BuiltValueNullFieldError('VkPhotoSize', 'width');
    }
    if (height == null) {
      throw new BuiltValueNullFieldError('VkPhotoSize', 'height');
    }
  }

  @override
  VkPhotoSize rebuild(void Function(VkPhotoSizeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VkPhotoSizeBuilder toBuilder() => new VkPhotoSizeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is VkPhotoSize &&
        src == other.src &&
        type == other.type &&
        width == other.width &&
        height == other.height;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, src.hashCode), type.hashCode), width.hashCode),
        height.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('VkPhotoSize')
          ..add('src', src)
          ..add('type', type)
          ..add('width', width)
          ..add('height', height))
        .toString();
  }
}

class VkPhotoSizeBuilder implements Builder<VkPhotoSize, VkPhotoSizeBuilder> {
  _$VkPhotoSize _$v;

  String _src;
  String get src => _$this._src;
  set src(String src) => _$this._src = src;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  int _width;
  int get width => _$this._width;
  set width(int width) => _$this._width = width;

  int _height;
  int get height => _$this._height;
  set height(int height) => _$this._height = height;

  VkPhotoSizeBuilder();

  VkPhotoSizeBuilder get _$this {
    if (_$v != null) {
      _src = _$v.src;
      _type = _$v.type;
      _width = _$v.width;
      _height = _$v.height;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(VkPhotoSize other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$VkPhotoSize;
  }

  @override
  void update(void Function(VkPhotoSizeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$VkPhotoSize build() {
    final _$result = _$v ??
        new _$VkPhotoSize._(src: src, type: type, width: width, height: height);
    replace(_$result);
    return _$result;
  }
}

class _$VkPhoto extends VkPhoto {
  @override
  final String id;
  @override
  final String ownerId;
  @override
  final String albumId;
  @override
  final String userId;
  @override
  final String text;
  @override
  final DateTime date;
  @override
  final BuiltList<VkPhotoSize> sizes;

  factory _$VkPhoto([void Function(VkPhotoBuilder) updates]) =>
      (new VkPhotoBuilder()..update(updates)).build();

  _$VkPhoto._(
      {this.id,
      this.ownerId,
      this.albumId,
      this.userId,
      this.text,
      this.date,
      this.sizes})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'id');
    }
    if (ownerId == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'ownerId');
    }
    if (albumId == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'albumId');
    }
    if (userId == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'userId');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'text');
    }
    if (date == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'date');
    }
    if (sizes == null) {
      throw new BuiltValueNullFieldError('VkPhoto', 'sizes');
    }
  }

  @override
  VkPhoto rebuild(void Function(VkPhotoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VkPhotoBuilder toBuilder() => new VkPhotoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is VkPhoto &&
        id == other.id &&
        ownerId == other.ownerId &&
        albumId == other.albumId &&
        userId == other.userId &&
        text == other.text &&
        date == other.date &&
        sizes == other.sizes;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), ownerId.hashCode),
                        albumId.hashCode),
                    userId.hashCode),
                text.hashCode),
            date.hashCode),
        sizes.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('VkPhoto')
          ..add('id', id)
          ..add('ownerId', ownerId)
          ..add('albumId', albumId)
          ..add('userId', userId)
          ..add('text', text)
          ..add('date', date)
          ..add('sizes', sizes))
        .toString();
  }
}

class VkPhotoBuilder implements Builder<VkPhoto, VkPhotoBuilder> {
  _$VkPhoto _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _ownerId;
  String get ownerId => _$this._ownerId;
  set ownerId(String ownerId) => _$this._ownerId = ownerId;

  String _albumId;
  String get albumId => _$this._albumId;
  set albumId(String albumId) => _$this._albumId = albumId;

  String _userId;
  String get userId => _$this._userId;
  set userId(String userId) => _$this._userId = userId;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  DateTime _date;
  DateTime get date => _$this._date;
  set date(DateTime date) => _$this._date = date;

  ListBuilder<VkPhotoSize> _sizes;
  ListBuilder<VkPhotoSize> get sizes =>
      _$this._sizes ??= new ListBuilder<VkPhotoSize>();
  set sizes(ListBuilder<VkPhotoSize> sizes) => _$this._sizes = sizes;

  VkPhotoBuilder();

  VkPhotoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _ownerId = _$v.ownerId;
      _albumId = _$v.albumId;
      _userId = _$v.userId;
      _text = _$v.text;
      _date = _$v.date;
      _sizes = _$v.sizes?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(VkPhoto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$VkPhoto;
  }

  @override
  void update(void Function(VkPhotoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$VkPhoto build() {
    _$VkPhoto _$result;
    try {
      _$result = _$v ??
          new _$VkPhoto._(
              id: id,
              ownerId: ownerId,
              albumId: albumId,
              userId: userId,
              text: text,
              date: date,
              sizes: sizes.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'sizes';
        sizes.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'VkPhoto', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
