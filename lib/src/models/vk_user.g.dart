// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vk_user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LastSeen> _$lastSeenSerializer = new _$LastSeenSerializer();
Serializer<VkUser> _$vkUserSerializer = new _$VkUserSerializer();

class _$LastSeenSerializer implements StructuredSerializer<LastSeen> {
  @override
  final Iterable<Type> types = const [LastSeen, _$LastSeen];
  @override
  final String wireName = 'LastSeen';

  @override
  Iterable<Object> serialize(Serializers serializers, LastSeen object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'time',
      serializers.serialize(object.time,
          specifiedType: const FullType(DateTime)),
      'platform',
      serializers.serialize(object.platform,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  LastSeen deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LastSeenBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'time':
          result.time = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'platform':
          result.platform = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$VkUserSerializer implements StructuredSerializer<VkUser> {
  @override
  final Iterable<Type> types = const [VkUser, _$VkUser];
  @override
  final String wireName = 'VkUser';

  @override
  Iterable<Object> serialize(Serializers serializers, VkUser object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'first_name',
      serializers.serialize(object.firstName,
          specifiedType: const FullType(String)),
      'last_name',
      serializers.serialize(object.lastName,
          specifiedType: const FullType(String)),
      'is_closed',
      serializers.serialize(object.isClosed,
          specifiedType: const FullType(bool)),
      'can_access_closed',
      serializers.serialize(object.canAccessClosed,
          specifiedType: const FullType(bool)),
    ];
    if (object.deactivated != null) {
      result
        ..add('deactivated')
        ..add(serializers.serialize(object.deactivated,
            specifiedType: const FullType(String)));
    }
    if (object.about != null) {
      result
        ..add('about')
        ..add(serializers.serialize(object.about,
            specifiedType: const FullType(String)));
    }
    if (object.activities != null) {
      result
        ..add('activities')
        ..add(serializers.serialize(object.activities,
            specifiedType: const FullType(String)));
    }
    if (object.bdate != null) {
      result
        ..add('bdate')
        ..add(serializers.serialize(object.bdate,
            specifiedType: const FullType(String)));
    }
    if (object.blacklisted != null) {
      result
        ..add('blacklisted')
        ..add(serializers.serialize(object.blacklisted,
            specifiedType: const FullType(bool)));
    }
    if (object.blacklistedByMe != null) {
      result
        ..add('blacklisted_by_me')
        ..add(serializers.serialize(object.blacklistedByMe,
            specifiedType: const FullType(bool)));
    }
    if (object.books != null) {
      result
        ..add('books')
        ..add(serializers.serialize(object.books,
            specifiedType: const FullType(String)));
    }
    if (object.canPost != null) {
      result
        ..add('can_post')
        ..add(serializers.serialize(object.canPost,
            specifiedType: const FullType(bool)));
    }
    if (object.canSeeAllPosts != null) {
      result
        ..add('can_see_all_posts')
        ..add(serializers.serialize(object.canSeeAllPosts,
            specifiedType: const FullType(bool)));
    }
    if (object.canSeeAudio != null) {
      result
        ..add('can_see_audio')
        ..add(serializers.serialize(object.canSeeAudio,
            specifiedType: const FullType(bool)));
    }
    if (object.canSendFriendRequest != null) {
      result
        ..add('can_send_friend_request')
        ..add(serializers.serialize(object.canSendFriendRequest,
            specifiedType: const FullType(bool)));
    }
    if (object.canWritePrivateMessage != null) {
      result
        ..add('can_write_private_message')
        ..add(serializers.serialize(object.canWritePrivateMessage,
            specifiedType: const FullType(bool)));
    }
    if (object.commonCount != null) {
      result
        ..add('common_count')
        ..add(serializers.serialize(object.commonCount,
            specifiedType: const FullType(int)));
    }
    if (object.connections != null) {
      result
        ..add('connections')
        ..add(serializers.serialize(object.connections,
            specifiedType: const FullType(
                Map, const [const FullType(String), const FullType(String)])));
    }
    if (object.domain != null) {
      result
        ..add('domain')
        ..add(serializers.serialize(object.domain,
            specifiedType: const FullType(String)));
    }
    if (object.followersCount != null) {
      result
        ..add('followers_count')
        ..add(serializers.serialize(object.followersCount,
            specifiedType: const FullType(int)));
    }
    if (object.friendStatus != null) {
      result
        ..add('friend_status')
        ..add(serializers.serialize(object.friendStatus,
            specifiedType: const FullType(int)));
    }
    if (object.games != null) {
      result
        ..add('games')
        ..add(serializers.serialize(object.games,
            specifiedType: const FullType(String)));
    }
    if (object.hasMobile != null) {
      result
        ..add('has_mobile')
        ..add(serializers.serialize(object.hasMobile,
            specifiedType: const FullType(bool)));
    }
    if (object.hasPhoto != null) {
      result
        ..add('has_photo')
        ..add(serializers.serialize(object.hasPhoto,
            specifiedType: const FullType(bool)));
    }
    if (object.homeTown != null) {
      result
        ..add('home_town')
        ..add(serializers.serialize(object.homeTown,
            specifiedType: const FullType(String)));
    }
    if (object.interests != null) {
      result
        ..add('interests')
        ..add(serializers.serialize(object.interests,
            specifiedType: const FullType(String)));
    }
    if (object.isFavorite != null) {
      result
        ..add('is_favorite')
        ..add(serializers.serialize(object.isFavorite,
            specifiedType: const FullType(bool)));
    }
    if (object.isFriend != null) {
      result
        ..add('is_friend')
        ..add(serializers.serialize(object.isFriend,
            specifiedType: const FullType(bool)));
    }
    if (object.isHiddenFromFeed != null) {
      result
        ..add('is_hidden_from_feed')
        ..add(serializers.serialize(object.isHiddenFromFeed,
            specifiedType: const FullType(bool)));
    }
    if (object.lastSeen != null) {
      result
        ..add('last_seen')
        ..add(serializers.serialize(object.lastSeen,
            specifiedType: const FullType(LastSeen)));
    }
    if (object.lists != null) {
      result
        ..add('lists')
        ..add(serializers.serialize(object.lists,
            specifiedType:
                const FullType(BuiltList, const [const FullType(int)])));
    }
    if (object.maidenName != null) {
      result
        ..add('maiden_name')
        ..add(serializers.serialize(object.maidenName,
            specifiedType: const FullType(String)));
    }
    if (object.movies != null) {
      result
        ..add('movies')
        ..add(serializers.serialize(object.movies,
            specifiedType: const FullType(String)));
    }
    if (object.music != null) {
      result
        ..add('music')
        ..add(serializers.serialize(object.music,
            specifiedType: const FullType(String)));
    }
    if (object.nickname != null) {
      result
        ..add('nickname')
        ..add(serializers.serialize(object.nickname,
            specifiedType: const FullType(String)));
    }
    if (object.online != null) {
      result
        ..add('online')
        ..add(serializers.serialize(object.online,
            specifiedType: const FullType(bool)));
    }
    if (object.photo50 != null) {
      result
        ..add('photo_50')
        ..add(serializers.serialize(object.photo50,
            specifiedType: const FullType(String)));
    }
    if (object.photo100 != null) {
      result
        ..add('photo_100')
        ..add(serializers.serialize(object.photo100,
            specifiedType: const FullType(String)));
    }
    if (object.photo200Orig != null) {
      result
        ..add('photo_200_orig')
        ..add(serializers.serialize(object.photo200Orig,
            specifiedType: const FullType(String)));
    }
    if (object.photo200 != null) {
      result
        ..add('photo_200')
        ..add(serializers.serialize(object.photo200,
            specifiedType: const FullType(String)));
    }
    if (object.photo400Orig != null) {
      result
        ..add('photo_400_orig')
        ..add(serializers.serialize(object.photo400Orig,
            specifiedType: const FullType(String)));
    }
    if (object.photoId != null) {
      result
        ..add('photo_id')
        ..add(serializers.serialize(object.photoId,
            specifiedType: const FullType(String)));
    }
    if (object.photoMax != null) {
      result
        ..add('photo_max')
        ..add(serializers.serialize(object.photoMax,
            specifiedType: const FullType(String)));
    }
    if (object.photoMaxOrig != null) {
      result
        ..add('photo_max_orig')
        ..add(serializers.serialize(object.photoMaxOrig,
            specifiedType: const FullType(String)));
    }
    if (object.quotes != null) {
      result
        ..add('quotes')
        ..add(serializers.serialize(object.quotes,
            specifiedType: const FullType(String)));
    }
    if (object.relation != null) {
      result
        ..add('relation')
        ..add(serializers.serialize(object.relation,
            specifiedType: const FullType(int)));
    }
    if (object.screenName != null) {
      result
        ..add('screen_name')
        ..add(serializers.serialize(object.screenName,
            specifiedType: const FullType(String)));
    }
    if (object.sex != null) {
      result
        ..add('sex')
        ..add(serializers.serialize(object.sex,
            specifiedType: const FullType(int)));
    }
    if (object.site != null) {
      result
        ..add('site')
        ..add(serializers.serialize(object.site,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    if (object.timezone != null) {
      result
        ..add('timezone')
        ..add(serializers.serialize(object.timezone,
            specifiedType: const FullType(int)));
    }
    if (object.trending != null) {
      result
        ..add('trending')
        ..add(serializers.serialize(object.trending,
            specifiedType: const FullType(bool)));
    }
    if (object.tv != null) {
      result
        ..add('tv')
        ..add(serializers.serialize(object.tv,
            specifiedType: const FullType(String)));
    }
    if (object.verified != null) {
      result
        ..add('verified')
        ..add(serializers.serialize(object.verified,
            specifiedType: const FullType(bool)));
    }
    if (object.wallDefault != null) {
      result
        ..add('wall_default')
        ..add(serializers.serialize(object.wallDefault,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  VkUser deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new VkUserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'first_name':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'last_name':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'deactivated':
          result.deactivated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'is_closed':
          result.isClosed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'can_access_closed':
          result.canAccessClosed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'about':
          result.about = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'activities':
          result.activities = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'bdate':
          result.bdate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'blacklisted':
          result.blacklisted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'blacklisted_by_me':
          result.blacklistedByMe = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'books':
          result.books = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'can_post':
          result.canPost = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'can_see_all_posts':
          result.canSeeAllPosts = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'can_see_audio':
          result.canSeeAudio = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'can_send_friend_request':
          result.canSendFriendRequest = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'can_write_private_message':
          result.canWritePrivateMessage = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'common_count':
          result.commonCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'connections':
          result.connections = serializers.deserialize(value,
              specifiedType: const FullType(Map, const [
                const FullType(String),
                const FullType(String)
              ])) as Map<String, String>;
          break;
        case 'domain':
          result.domain = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'followers_count':
          result.followersCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'friend_status':
          result.friendStatus = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'games':
          result.games = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'has_mobile':
          result.hasMobile = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'has_photo':
          result.hasPhoto = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'home_town':
          result.homeTown = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'interests':
          result.interests = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'is_favorite':
          result.isFavorite = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'is_friend':
          result.isFriend = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'is_hidden_from_feed':
          result.isHiddenFromFeed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'last_seen':
          result.lastSeen.replace(serializers.deserialize(value,
              specifiedType: const FullType(LastSeen)) as LastSeen);
          break;
        case 'lists':
          result.lists.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(int)]))
              as BuiltList<dynamic>);
          break;
        case 'maiden_name':
          result.maidenName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'movies':
          result.movies = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'music':
          result.music = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nickname':
          result.nickname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'online':
          result.online = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'photo_50':
          result.photo50 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_100':
          result.photo100 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_200_orig':
          result.photo200Orig = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_200':
          result.photo200 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_400_orig':
          result.photo400Orig = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_id':
          result.photoId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_max':
          result.photoMax = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'photo_max_orig':
          result.photoMaxOrig = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'quotes':
          result.quotes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'relation':
          result.relation = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'screen_name':
          result.screenName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sex':
          result.sex = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'site':
          result.site = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'timezone':
          result.timezone = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'trending':
          result.trending = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'tv':
          result.tv = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'verified':
          result.verified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'wall_default':
          result.wallDefault = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$LastSeen extends LastSeen {
  @override
  final DateTime time;
  @override
  final int platform;

  factory _$LastSeen([void Function(LastSeenBuilder) updates]) =>
      (new LastSeenBuilder()..update(updates)).build();

  _$LastSeen._({this.time, this.platform}) : super._() {
    if (time == null) {
      throw new BuiltValueNullFieldError('LastSeen', 'time');
    }
    if (platform == null) {
      throw new BuiltValueNullFieldError('LastSeen', 'platform');
    }
  }

  @override
  LastSeen rebuild(void Function(LastSeenBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LastSeenBuilder toBuilder() => new LastSeenBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LastSeen &&
        time == other.time &&
        platform == other.platform;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, time.hashCode), platform.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LastSeen')
          ..add('time', time)
          ..add('platform', platform))
        .toString();
  }
}

class LastSeenBuilder implements Builder<LastSeen, LastSeenBuilder> {
  _$LastSeen _$v;

  DateTime _time;
  DateTime get time => _$this._time;
  set time(DateTime time) => _$this._time = time;

  int _platform;
  int get platform => _$this._platform;
  set platform(int platform) => _$this._platform = platform;

  LastSeenBuilder();

  LastSeenBuilder get _$this {
    if (_$v != null) {
      _time = _$v.time;
      _platform = _$v.platform;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LastSeen other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LastSeen;
  }

  @override
  void update(void Function(LastSeenBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LastSeen build() {
    final _$result = _$v ?? new _$LastSeen._(time: time, platform: platform);
    replace(_$result);
    return _$result;
  }
}

class _$VkUser extends VkUser {
  @override
  final int id;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String deactivated;
  @override
  final bool isClosed;
  @override
  final bool canAccessClosed;
  @override
  final String about;
  @override
  final String activities;
  @override
  final String bdate;
  @override
  final bool blacklisted;
  @override
  final bool blacklistedByMe;
  @override
  final String books;
  @override
  final bool canPost;
  @override
  final bool canSeeAllPosts;
  @override
  final bool canSeeAudio;
  @override
  final bool canSendFriendRequest;
  @override
  final bool canWritePrivateMessage;
  @override
  final int commonCount;
  @override
  final Map<String, String> connections;
  @override
  final String domain;
  @override
  final int followersCount;
  @override
  final int friendStatus;
  @override
  final String games;
  @override
  final bool hasMobile;
  @override
  final bool hasPhoto;
  @override
  final String homeTown;
  @override
  final String interests;
  @override
  final bool isFavorite;
  @override
  final bool isFriend;
  @override
  final bool isHiddenFromFeed;
  @override
  final LastSeen lastSeen;
  @override
  final BuiltList<int> lists;
  @override
  final String maidenName;
  @override
  final String movies;
  @override
  final String music;
  @override
  final String nickname;
  @override
  final bool online;
  @override
  final String photo50;
  @override
  final String photo100;
  @override
  final String photo200Orig;
  @override
  final String photo200;
  @override
  final String photo400Orig;
  @override
  final String photoId;
  @override
  final String photoMax;
  @override
  final String photoMaxOrig;
  @override
  final String quotes;
  @override
  final int relation;
  @override
  final String screenName;
  @override
  final int sex;
  @override
  final String site;
  @override
  final String status;
  @override
  final int timezone;
  @override
  final bool trending;
  @override
  final String tv;
  @override
  final bool verified;
  @override
  final String wallDefault;

  factory _$VkUser([void Function(VkUserBuilder) updates]) =>
      (new VkUserBuilder()..update(updates)).build();

  _$VkUser._(
      {this.id,
      this.firstName,
      this.lastName,
      this.deactivated,
      this.isClosed,
      this.canAccessClosed,
      this.about,
      this.activities,
      this.bdate,
      this.blacklisted,
      this.blacklistedByMe,
      this.books,
      this.canPost,
      this.canSeeAllPosts,
      this.canSeeAudio,
      this.canSendFriendRequest,
      this.canWritePrivateMessage,
      this.commonCount,
      this.connections,
      this.domain,
      this.followersCount,
      this.friendStatus,
      this.games,
      this.hasMobile,
      this.hasPhoto,
      this.homeTown,
      this.interests,
      this.isFavorite,
      this.isFriend,
      this.isHiddenFromFeed,
      this.lastSeen,
      this.lists,
      this.maidenName,
      this.movies,
      this.music,
      this.nickname,
      this.online,
      this.photo50,
      this.photo100,
      this.photo200Orig,
      this.photo200,
      this.photo400Orig,
      this.photoId,
      this.photoMax,
      this.photoMaxOrig,
      this.quotes,
      this.relation,
      this.screenName,
      this.sex,
      this.site,
      this.status,
      this.timezone,
      this.trending,
      this.tv,
      this.verified,
      this.wallDefault})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('VkUser', 'id');
    }
    if (firstName == null) {
      throw new BuiltValueNullFieldError('VkUser', 'firstName');
    }
    if (lastName == null) {
      throw new BuiltValueNullFieldError('VkUser', 'lastName');
    }
    if (isClosed == null) {
      throw new BuiltValueNullFieldError('VkUser', 'isClosed');
    }
    if (canAccessClosed == null) {
      throw new BuiltValueNullFieldError('VkUser', 'canAccessClosed');
    }
  }

  @override
  VkUser rebuild(void Function(VkUserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VkUserBuilder toBuilder() => new VkUserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is VkUser &&
        id == other.id &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        deactivated == other.deactivated &&
        isClosed == other.isClosed &&
        canAccessClosed == other.canAccessClosed &&
        about == other.about &&
        activities == other.activities &&
        bdate == other.bdate &&
        blacklisted == other.blacklisted &&
        blacklistedByMe == other.blacklistedByMe &&
        books == other.books &&
        canPost == other.canPost &&
        canSeeAllPosts == other.canSeeAllPosts &&
        canSeeAudio == other.canSeeAudio &&
        canSendFriendRequest == other.canSendFriendRequest &&
        canWritePrivateMessage == other.canWritePrivateMessage &&
        commonCount == other.commonCount &&
        connections == other.connections &&
        domain == other.domain &&
        followersCount == other.followersCount &&
        friendStatus == other.friendStatus &&
        games == other.games &&
        hasMobile == other.hasMobile &&
        hasPhoto == other.hasPhoto &&
        homeTown == other.homeTown &&
        interests == other.interests &&
        isFavorite == other.isFavorite &&
        isFriend == other.isFriend &&
        isHiddenFromFeed == other.isHiddenFromFeed &&
        lastSeen == other.lastSeen &&
        lists == other.lists &&
        maidenName == other.maidenName &&
        movies == other.movies &&
        music == other.music &&
        nickname == other.nickname &&
        online == other.online &&
        photo50 == other.photo50 &&
        photo100 == other.photo100 &&
        photo200Orig == other.photo200Orig &&
        photo200 == other.photo200 &&
        photo400Orig == other.photo400Orig &&
        photoId == other.photoId &&
        photoMax == other.photoMax &&
        photoMaxOrig == other.photoMaxOrig &&
        quotes == other.quotes &&
        relation == other.relation &&
        screenName == other.screenName &&
        sex == other.sex &&
        site == other.site &&
        status == other.status &&
        timezone == other.timezone &&
        trending == other.trending &&
        tv == other.tv &&
        verified == other.verified &&
        wallDefault == other.wallDefault;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            $jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc(0, id.hashCode), firstName.hashCode), lastName.hashCode), deactivated.hashCode), isClosed.hashCode), canAccessClosed.hashCode), about.hashCode), activities.hashCode), bdate.hashCode), blacklisted.hashCode), blacklistedByMe.hashCode), books.hashCode), canPost.hashCode), canSeeAllPosts.hashCode), canSeeAudio.hashCode), canSendFriendRequest.hashCode), canWritePrivateMessage.hashCode), commonCount.hashCode), connections.hashCode), domain.hashCode), followersCount.hashCode), friendStatus.hashCode), games.hashCode), hasMobile.hashCode), hasPhoto.hashCode), homeTown.hashCode), interests.hashCode), isFavorite.hashCode), isFriend.hashCode), isHiddenFromFeed.hashCode), lastSeen.hashCode), lists.hashCode), maidenName.hashCode), movies.hashCode), music.hashCode), nickname.hashCode), online.hashCode),
                                                                                photo50.hashCode),
                                                                            photo100.hashCode),
                                                                        photo200Orig.hashCode),
                                                                    photo200.hashCode),
                                                                photo400Orig.hashCode),
                                                            photoId.hashCode),
                                                        photoMax.hashCode),
                                                    photoMaxOrig.hashCode),
                                                quotes.hashCode),
                                            relation.hashCode),
                                        screenName.hashCode),
                                    sex.hashCode),
                                site.hashCode),
                            status.hashCode),
                        timezone.hashCode),
                    trending.hashCode),
                tv.hashCode),
            verified.hashCode),
        wallDefault.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('VkUser')
          ..add('id', id)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('deactivated', deactivated)
          ..add('isClosed', isClosed)
          ..add('canAccessClosed', canAccessClosed)
          ..add('about', about)
          ..add('activities', activities)
          ..add('bdate', bdate)
          ..add('blacklisted', blacklisted)
          ..add('blacklistedByMe', blacklistedByMe)
          ..add('books', books)
          ..add('canPost', canPost)
          ..add('canSeeAllPosts', canSeeAllPosts)
          ..add('canSeeAudio', canSeeAudio)
          ..add('canSendFriendRequest', canSendFriendRequest)
          ..add('canWritePrivateMessage', canWritePrivateMessage)
          ..add('commonCount', commonCount)
          ..add('connections', connections)
          ..add('domain', domain)
          ..add('followersCount', followersCount)
          ..add('friendStatus', friendStatus)
          ..add('games', games)
          ..add('hasMobile', hasMobile)
          ..add('hasPhoto', hasPhoto)
          ..add('homeTown', homeTown)
          ..add('interests', interests)
          ..add('isFavorite', isFavorite)
          ..add('isFriend', isFriend)
          ..add('isHiddenFromFeed', isHiddenFromFeed)
          ..add('lastSeen', lastSeen)
          ..add('lists', lists)
          ..add('maidenName', maidenName)
          ..add('movies', movies)
          ..add('music', music)
          ..add('nickname', nickname)
          ..add('online', online)
          ..add('photo50', photo50)
          ..add('photo100', photo100)
          ..add('photo200Orig', photo200Orig)
          ..add('photo200', photo200)
          ..add('photo400Orig', photo400Orig)
          ..add('photoId', photoId)
          ..add('photoMax', photoMax)
          ..add('photoMaxOrig', photoMaxOrig)
          ..add('quotes', quotes)
          ..add('relation', relation)
          ..add('screenName', screenName)
          ..add('sex', sex)
          ..add('site', site)
          ..add('status', status)
          ..add('timezone', timezone)
          ..add('trending', trending)
          ..add('tv', tv)
          ..add('verified', verified)
          ..add('wallDefault', wallDefault))
        .toString();
  }
}

class VkUserBuilder implements Builder<VkUser, VkUserBuilder> {
  _$VkUser _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _firstName;
  String get firstName => _$this._firstName;
  set firstName(String firstName) => _$this._firstName = firstName;

  String _lastName;
  String get lastName => _$this._lastName;
  set lastName(String lastName) => _$this._lastName = lastName;

  String _deactivated;
  String get deactivated => _$this._deactivated;
  set deactivated(String deactivated) => _$this._deactivated = deactivated;

  bool _isClosed;
  bool get isClosed => _$this._isClosed;
  set isClosed(bool isClosed) => _$this._isClosed = isClosed;

  bool _canAccessClosed;
  bool get canAccessClosed => _$this._canAccessClosed;
  set canAccessClosed(bool canAccessClosed) =>
      _$this._canAccessClosed = canAccessClosed;

  String _about;
  String get about => _$this._about;
  set about(String about) => _$this._about = about;

  String _activities;
  String get activities => _$this._activities;
  set activities(String activities) => _$this._activities = activities;

  String _bdate;
  String get bdate => _$this._bdate;
  set bdate(String bdate) => _$this._bdate = bdate;

  bool _blacklisted;
  bool get blacklisted => _$this._blacklisted;
  set blacklisted(bool blacklisted) => _$this._blacklisted = blacklisted;

  bool _blacklistedByMe;
  bool get blacklistedByMe => _$this._blacklistedByMe;
  set blacklistedByMe(bool blacklistedByMe) =>
      _$this._blacklistedByMe = blacklistedByMe;

  String _books;
  String get books => _$this._books;
  set books(String books) => _$this._books = books;

  bool _canPost;
  bool get canPost => _$this._canPost;
  set canPost(bool canPost) => _$this._canPost = canPost;

  bool _canSeeAllPosts;
  bool get canSeeAllPosts => _$this._canSeeAllPosts;
  set canSeeAllPosts(bool canSeeAllPosts) =>
      _$this._canSeeAllPosts = canSeeAllPosts;

  bool _canSeeAudio;
  bool get canSeeAudio => _$this._canSeeAudio;
  set canSeeAudio(bool canSeeAudio) => _$this._canSeeAudio = canSeeAudio;

  bool _canSendFriendRequest;
  bool get canSendFriendRequest => _$this._canSendFriendRequest;
  set canSendFriendRequest(bool canSendFriendRequest) =>
      _$this._canSendFriendRequest = canSendFriendRequest;

  bool _canWritePrivateMessage;
  bool get canWritePrivateMessage => _$this._canWritePrivateMessage;
  set canWritePrivateMessage(bool canWritePrivateMessage) =>
      _$this._canWritePrivateMessage = canWritePrivateMessage;

  int _commonCount;
  int get commonCount => _$this._commonCount;
  set commonCount(int commonCount) => _$this._commonCount = commonCount;

  Map<String, String> _connections;
  Map<String, String> get connections => _$this._connections;
  set connections(Map<String, String> connections) =>
      _$this._connections = connections;

  String _domain;
  String get domain => _$this._domain;
  set domain(String domain) => _$this._domain = domain;

  int _followersCount;
  int get followersCount => _$this._followersCount;
  set followersCount(int followersCount) =>
      _$this._followersCount = followersCount;

  int _friendStatus;
  int get friendStatus => _$this._friendStatus;
  set friendStatus(int friendStatus) => _$this._friendStatus = friendStatus;

  String _games;
  String get games => _$this._games;
  set games(String games) => _$this._games = games;

  bool _hasMobile;
  bool get hasMobile => _$this._hasMobile;
  set hasMobile(bool hasMobile) => _$this._hasMobile = hasMobile;

  bool _hasPhoto;
  bool get hasPhoto => _$this._hasPhoto;
  set hasPhoto(bool hasPhoto) => _$this._hasPhoto = hasPhoto;

  String _homeTown;
  String get homeTown => _$this._homeTown;
  set homeTown(String homeTown) => _$this._homeTown = homeTown;

  String _interests;
  String get interests => _$this._interests;
  set interests(String interests) => _$this._interests = interests;

  bool _isFavorite;
  bool get isFavorite => _$this._isFavorite;
  set isFavorite(bool isFavorite) => _$this._isFavorite = isFavorite;

  bool _isFriend;
  bool get isFriend => _$this._isFriend;
  set isFriend(bool isFriend) => _$this._isFriend = isFriend;

  bool _isHiddenFromFeed;
  bool get isHiddenFromFeed => _$this._isHiddenFromFeed;
  set isHiddenFromFeed(bool isHiddenFromFeed) =>
      _$this._isHiddenFromFeed = isHiddenFromFeed;

  LastSeenBuilder _lastSeen;
  LastSeenBuilder get lastSeen => _$this._lastSeen ??= new LastSeenBuilder();
  set lastSeen(LastSeenBuilder lastSeen) => _$this._lastSeen = lastSeen;

  ListBuilder<int> _lists;
  ListBuilder<int> get lists => _$this._lists ??= new ListBuilder<int>();
  set lists(ListBuilder<int> lists) => _$this._lists = lists;

  String _maidenName;
  String get maidenName => _$this._maidenName;
  set maidenName(String maidenName) => _$this._maidenName = maidenName;

  String _movies;
  String get movies => _$this._movies;
  set movies(String movies) => _$this._movies = movies;

  String _music;
  String get music => _$this._music;
  set music(String music) => _$this._music = music;

  String _nickname;
  String get nickname => _$this._nickname;
  set nickname(String nickname) => _$this._nickname = nickname;

  bool _online;
  bool get online => _$this._online;
  set online(bool online) => _$this._online = online;

  String _photo50;
  String get photo50 => _$this._photo50;
  set photo50(String photo50) => _$this._photo50 = photo50;

  String _photo100;
  String get photo100 => _$this._photo100;
  set photo100(String photo100) => _$this._photo100 = photo100;

  String _photo200Orig;
  String get photo200Orig => _$this._photo200Orig;
  set photo200Orig(String photo200Orig) => _$this._photo200Orig = photo200Orig;

  String _photo200;
  String get photo200 => _$this._photo200;
  set photo200(String photo200) => _$this._photo200 = photo200;

  String _photo400Orig;
  String get photo400Orig => _$this._photo400Orig;
  set photo400Orig(String photo400Orig) => _$this._photo400Orig = photo400Orig;

  String _photoId;
  String get photoId => _$this._photoId;
  set photoId(String photoId) => _$this._photoId = photoId;

  String _photoMax;
  String get photoMax => _$this._photoMax;
  set photoMax(String photoMax) => _$this._photoMax = photoMax;

  String _photoMaxOrig;
  String get photoMaxOrig => _$this._photoMaxOrig;
  set photoMaxOrig(String photoMaxOrig) => _$this._photoMaxOrig = photoMaxOrig;

  String _quotes;
  String get quotes => _$this._quotes;
  set quotes(String quotes) => _$this._quotes = quotes;

  int _relation;
  int get relation => _$this._relation;
  set relation(int relation) => _$this._relation = relation;

  String _screenName;
  String get screenName => _$this._screenName;
  set screenName(String screenName) => _$this._screenName = screenName;

  int _sex;
  int get sex => _$this._sex;
  set sex(int sex) => _$this._sex = sex;

  String _site;
  String get site => _$this._site;
  set site(String site) => _$this._site = site;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  int _timezone;
  int get timezone => _$this._timezone;
  set timezone(int timezone) => _$this._timezone = timezone;

  bool _trending;
  bool get trending => _$this._trending;
  set trending(bool trending) => _$this._trending = trending;

  String _tv;
  String get tv => _$this._tv;
  set tv(String tv) => _$this._tv = tv;

  bool _verified;
  bool get verified => _$this._verified;
  set verified(bool verified) => _$this._verified = verified;

  String _wallDefault;
  String get wallDefault => _$this._wallDefault;
  set wallDefault(String wallDefault) => _$this._wallDefault = wallDefault;

  VkUserBuilder();

  VkUserBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _firstName = _$v.firstName;
      _lastName = _$v.lastName;
      _deactivated = _$v.deactivated;
      _isClosed = _$v.isClosed;
      _canAccessClosed = _$v.canAccessClosed;
      _about = _$v.about;
      _activities = _$v.activities;
      _bdate = _$v.bdate;
      _blacklisted = _$v.blacklisted;
      _blacklistedByMe = _$v.blacklistedByMe;
      _books = _$v.books;
      _canPost = _$v.canPost;
      _canSeeAllPosts = _$v.canSeeAllPosts;
      _canSeeAudio = _$v.canSeeAudio;
      _canSendFriendRequest = _$v.canSendFriendRequest;
      _canWritePrivateMessage = _$v.canWritePrivateMessage;
      _commonCount = _$v.commonCount;
      _connections = _$v.connections;
      _domain = _$v.domain;
      _followersCount = _$v.followersCount;
      _friendStatus = _$v.friendStatus;
      _games = _$v.games;
      _hasMobile = _$v.hasMobile;
      _hasPhoto = _$v.hasPhoto;
      _homeTown = _$v.homeTown;
      _interests = _$v.interests;
      _isFavorite = _$v.isFavorite;
      _isFriend = _$v.isFriend;
      _isHiddenFromFeed = _$v.isHiddenFromFeed;
      _lastSeen = _$v.lastSeen?.toBuilder();
      _lists = _$v.lists?.toBuilder();
      _maidenName = _$v.maidenName;
      _movies = _$v.movies;
      _music = _$v.music;
      _nickname = _$v.nickname;
      _online = _$v.online;
      _photo50 = _$v.photo50;
      _photo100 = _$v.photo100;
      _photo200Orig = _$v.photo200Orig;
      _photo200 = _$v.photo200;
      _photo400Orig = _$v.photo400Orig;
      _photoId = _$v.photoId;
      _photoMax = _$v.photoMax;
      _photoMaxOrig = _$v.photoMaxOrig;
      _quotes = _$v.quotes;
      _relation = _$v.relation;
      _screenName = _$v.screenName;
      _sex = _$v.sex;
      _site = _$v.site;
      _status = _$v.status;
      _timezone = _$v.timezone;
      _trending = _$v.trending;
      _tv = _$v.tv;
      _verified = _$v.verified;
      _wallDefault = _$v.wallDefault;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(VkUser other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$VkUser;
  }

  @override
  void update(void Function(VkUserBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$VkUser build() {
    _$VkUser _$result;
    try {
      _$result = _$v ??
          new _$VkUser._(
              id: id,
              firstName: firstName,
              lastName: lastName,
              deactivated: deactivated,
              isClosed: isClosed,
              canAccessClosed: canAccessClosed,
              about: about,
              activities: activities,
              bdate: bdate,
              blacklisted: blacklisted,
              blacklistedByMe: blacklistedByMe,
              books: books,
              canPost: canPost,
              canSeeAllPosts: canSeeAllPosts,
              canSeeAudio: canSeeAudio,
              canSendFriendRequest: canSendFriendRequest,
              canWritePrivateMessage: canWritePrivateMessage,
              commonCount: commonCount,
              connections: connections,
              domain: domain,
              followersCount: followersCount,
              friendStatus: friendStatus,
              games: games,
              hasMobile: hasMobile,
              hasPhoto: hasPhoto,
              homeTown: homeTown,
              interests: interests,
              isFavorite: isFavorite,
              isFriend: isFriend,
              isHiddenFromFeed: isHiddenFromFeed,
              lastSeen: _lastSeen?.build(),
              lists: _lists?.build(),
              maidenName: maidenName,
              movies: movies,
              music: music,
              nickname: nickname,
              online: online,
              photo50: photo50,
              photo100: photo100,
              photo200Orig: photo200Orig,
              photo200: photo200,
              photo400Orig: photo400Orig,
              photoId: photoId,
              photoMax: photoMax,
              photoMaxOrig: photoMaxOrig,
              quotes: quotes,
              relation: relation,
              screenName: screenName,
              sex: sex,
              site: site,
              status: status,
              timezone: timezone,
              trending: trending,
              tv: tv,
              verified: verified,
              wallDefault: wallDefault);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'lastSeen';
        _lastSeen?.build();
        _$failedField = 'lists';
        _lists?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'VkUser', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
