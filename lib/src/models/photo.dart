import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'photo.g.dart';


abstract class VkPhotoSize implements Built<VkPhotoSize, VkPhotoSizeBuilder> {
  static const List<String> types = [
    "s", "m", "x", "o", "p", "q", "r", "y", "z", "w"
  ];
  String get src;
  String get type;
  int get width;
  int get height;

  static Serializer<VkPhotoSize> get serializer => _$vkPhotoSizeSerializer;

  VkPhotoSize._();
  factory VkPhotoSize([updates(VkPhotoSizeBuilder b)]) = _$VkPhotoSize;
  factory VkPhotoSize.fromMap(Map<String, dynamic> map) => standardSerializers.deserializeWith(serializer, map);
  Map<String, dynamic> toMap() => standardSerializers.serializeWith(serializer, this);
}

abstract class VkPhoto implements Built<VkPhoto, VkPhotoBuilder> {
  String get id;
  
  @BuiltValueField(wireName: 'owner_id')
  String get ownerId;

  @BuiltValueField(wireName: 'album_id')
  String get albumId;

  @BuiltValueField(wireName: 'user_id')
  String get userId;
  String get text;
  DateTime get date;
  BuiltList<VkPhotoSize> get sizes;
  

  static Serializer<VkPhoto> get serializer => _$vkPhotoSerializer;
  VkPhoto._();
  factory VkPhoto([updates(VkPhotoBuilder b)]) = _$VkPhoto;
  factory VkPhoto.fromMap(Map<String, dynamic> map) => standardSerializers.deserializeWith(serializer, map);
  Map<String, dynamic> toMap() => standardSerializers.serializeWith(serializer, this);
}