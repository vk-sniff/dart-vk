
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'auth_info.g.dart';
abstract class AuthInfo implements Built<AuthInfo, AuthInfoBuilder> {
  AuthInfo._();
  factory AuthInfo([void Function(AuthInfoBuilder) updates]) = _$AuthInfo;
  static Serializer<AuthInfo> get serializer => _$authInfoSerializer;

  @BuiltValueField(wireName: 'access_token')
  String get accessToken;

  @BuiltValueField(wireName: 'user_id')
  int get id;

  factory AuthInfo.fromMap(Map<String, dynamic> map) => standardSerializers.deserializeWith(serializer, map);
  Map<String, dynamic> toMap() => standardSerializers.serializeWith(serializer, this);
}