import 'package:dart_vk/src/client.dart';
import 'package:dart_vk/src/utils.dart';
import 'package:dart_vk/vk.dart';

class PhotosApi {
  VkApiClient _client;
  String _token;

  PhotosApi(VkApiClient client, String token): 
    _client = client, 
    _token = token;

  Future<CountAndList<VkPhoto>> get({
    int ownerId,
    int albumId,
    List<int> photoIds,
    bool rev,
    bool extended,
    String feedType,
    int feed,
    bool photoSizes,
    int offset,
    int count
  }) async{
    final response = await _client.get('photos.get',
      queryParameters: removeNullsFromMap({
        'owner_id': ownerId,
        'album_id': albumId,
        'photo_ids': photoIds?.join(','),
        'rev': rev,
        'extended': extended,
        'feed_type': feedType,
        'feed': feed,
        'photo_sizes': photoSizes,
        'offset': offset,
        'count': count
      }),
      token: _token
    );
    return CountAndList.fromMap(castToJsonMap(response.data['response']), VkPhoto.serializer);
  }
}