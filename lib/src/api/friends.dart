import 'package:dart_vk/src/client.dart';
import 'package:dart_vk/src/utils.dart';
import 'package:dart_vk/vk.dart';

class FriendsApi {
  VkApiClient _client;
  String _token;

  FriendsApi(VkApiClient client, String token): 
    _client = client, 
    _token = token;

  Future<CountAndList<VkUser>> get({
    int userId,
    String order,
    int listId,
    int count,
    int offset,
    List<String> fields,
    String nameCase,
    String ref,
  }) async{
    final response = await _client.get('friends.get',
      queryParameters: removeNullsFromMap({
        'user_id': userId,
        'order': order,
        'list_id': listId,
        'count': count,
        'offset': offset,
        'fields': fields?.join(','),
        'name_case': nameCase,
        'ref': ref,
      }),
      token: _token
    );
    return CountAndList.fromMap(castToJsonMap(response.data['response']), VkUser.serializer);
  }
}