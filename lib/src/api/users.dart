import 'package:dart_vk/src/client.dart';
import 'package:dart_vk/src/utils.dart';
import 'package:dart_vk/vk.dart';

class UsersApi {
  VkApiClient _client;
  String _token;

  UsersApi(VkApiClient client, String token): 
    _client = client, 
    _token = token;

  Future<List<VkUser>> get({
    List<int> userIds,
    List<String> fields,
    String nameCase,
  }) async{
    final response = await _client.get('users.get',
      queryParameters: removeNullsFromMap({
        'user_ids': userIds?.join(','),
        'fields': fields?.join(','),
        'name_case': nameCase,
      }),
      token: _token
    );

    return ListDeserializator.deserialize(response.data['response'], VkUser.serializer);
  }
}