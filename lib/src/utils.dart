Map<TKey, TValue> removeNullsFromMap<TKey, TValue>(Map<TKey, TValue> source) {
  source.removeWhere((k, v) => v == null);
  return source;
}
Map<TKey, TValue> castMap<TKey, TValue>(Map<TKey, TValue> source) {
  source.removeWhere((k, v) => v == null);
  return source;
}
Map<String, dynamic> castToJsonMap(dynamic map) {
  return (map as Map<dynamic, dynamic>).cast<String, dynamic>();
}