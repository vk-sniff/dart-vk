import 'vk_exception.dart';

class VkApiException extends VkException {
  final int errorCode;
  final String errorMessage;

  final Map<String, dynamic> source;

  VkApiException(this.errorCode, this.errorMessage, this.source): super();
  
  factory VkApiException.parse(Map<String, dynamic> map) {
    return VkApiException(
      map['error']['error_code'], map['error']['error_msg'], map
    );
  }
}
