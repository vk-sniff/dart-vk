import 'vk_exception.dart';

class VkAuthException extends VkException {
  final String error;
  final Map<String, dynamic> source;

  VkAuthException(this.error, this.source): super();
  
  factory VkAuthException.parse(Map<String, dynamic> map) {
    String error = map['error'];
    switch(error) {
      case TwoFactorRequiredVkException.tfaError:
        return TwoFactorRequiredVkException.fromMap(map);

      case CaptchaRequiredVkException.captchaError:
        return CaptchaRequiredVkException.fromMap(map);

      case WrongLoginDataVkException.clientError:
        return WrongLoginDataVkException.fromMap(map);

      case InvalidRequestException.requestError:
        return InvalidRequestException.fromMap(map);
      
      default: return VkAuthException(error, map);
    }
  }
}

class TwoFactorRequiredVkException extends VkAuthException {
  static const String tfaError = 'need_validation';
  final String validationType;
  final String validationSid;
  final String phoneMask;
  final String errorDescription;
  final String redirectUrl;

  TwoFactorRequiredVkException({
    Map<String, dynamic> source,
    this.validationType,
    this.validationSid,
    this.phoneMask,
    this.errorDescription,
    this.redirectUrl
  }): super(tfaError, source);

  TwoFactorRequiredVkException.fromMap(Map<String, dynamic> map):
    this(
      source: map,
      validationType: map['validation_type'],
      validationSid: map['validation_sid'],
      phoneMask: map['phone_mask'],
      errorDescription: map['error_description'],
      redirectUrl: map['redirect_url']
    );
}

class CaptchaRequiredVkException extends VkAuthException {
  static const String captchaError = 'need_captcha';
  final String captchaSid;
  final String captchaImageUrl;
  CaptchaRequiredVkException({
    Map<String, dynamic> source,
    this.captchaSid, 
    this.captchaImageUrl
  }): super(captchaError, source);

  CaptchaRequiredVkException.fromMap(Map<String, dynamic> map):
    this(
      source: map,
      captchaSid: map['captcha_sid'],
      captchaImageUrl: map['captcha_img']
    );
}

class WrongLoginDataVkException extends VkAuthException {
  static const String clientError = 'invalid_client';
  final String errorType;
  final String errorDescription;
  WrongLoginDataVkException({
    Map<String, dynamic> source,
    this.errorType, 
    this.errorDescription
  }): super(clientError, source);

  WrongLoginDataVkException.fromMap(Map<String, dynamic> map):
    this(
      source: map,
      errorType: map['error_type'],
      errorDescription: map['error_description']
    );
}

class InvalidRequestException extends VkAuthException {
  static const String requestError = 'invalid_request';
  final String errorType;
  final String errorDescription;
  
  InvalidRequestException({
    Map<String, dynamic> source,
    this.errorType, 
    this.errorDescription
  }): super(requestError, source);

  factory InvalidRequestException.fromMap(
    Map<String, dynamic> map
  ) {
    String errorType = map['error_type'];
    switch(errorType) {
      case WrongOtpException.wrongOtpErrorType: 
        return WrongOtpException.fromMap(map);
      default:
        return InvalidRequestException(
          errorDescription: map['error_description'],
          errorType: map['error_type']
        );
    }
  }
}

class WrongOtpException extends InvalidRequestException {
  static const String wrongOtpErrorType = 'wrong_otp';
  WrongOtpException({
    Map<String, dynamic> source,
    String errorDescription
  }): super(
    source: source,
    errorType: wrongOtpErrorType,
    errorDescription: errorDescription
  );

  WrongOtpException.fromMap(Map<String, dynamic> map):
    this(
      source: map,
      errorDescription: map['error_description']
    );
}