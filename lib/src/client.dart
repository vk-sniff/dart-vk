import 'package:dart_vk/vk.dart';
import 'package:dio/dio.dart';

import 'endpoints.dart';

abstract class VkClient {
  Dio dio;
  Exception Function(Map<String, dynamic> map) parseError;
  Map<String, String> get baseQueryParameters => null;
  
  VkClient(String url, this.parseError): 
    dio = Dio( 
      BaseOptions(
        baseUrl: url,
        validateStatus: (code) => true
      )
    );

  Future<Response> get(String path, { Map<String, dynamic> queryParameters, String token }) async {
    final resp = await dio.get<Map<String, dynamic>>(path, 
      queryParameters: {
        'access_token': token,
        if(baseQueryParameters != null)
          ...baseQueryParameters,
        ...queryParameters,
      }
    );
    if(resp.data["error"] != null) {
      throw parseError(resp.data);
    }
    return resp;
  }
}

class VkAuthClient extends VkClient {
  VkAuthClient(): super(authEndpoint, (m) => VkAuthException.parse(m));
}

class VkApiClient extends VkClient {
  VkApiClient(): super(apiEndpoint, (m) => VkApiException.parse(m));
  @override
  Map<String, String> get baseQueryParameters => {
    'v': '5.101'
  };
}