import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:dart_vk/src/models/serializers.dart';


class CountAndList<T> {
  final List<T> items;
  final int count;
  final Serializer<T> _serializer;

  CountAndList({
    this.count, 
    this.items,
    Serializer<T> serializer
  }): _serializer = serializer;  
  
  CountAndList.fromMap(Map<String, dynamic> map, Serializer<T> serializer):
    _serializer = serializer,
    count = map['count'],
    items = map['items'] == null 
      ? null
      : (map['items'] as List)
        .map((i) => standardSerializers.deserializeWith(serializer, i))
        .toList();

  Map<String, dynamic> toMap() => {
    'count': count,
    'items': items?.map((i) => standardSerializers.serializeWith(_serializer, i))
  };
}

