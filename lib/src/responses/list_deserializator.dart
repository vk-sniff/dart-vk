import 'package:built_value/serializer.dart';
import 'package:dart_vk/src/models/serializers.dart';


abstract class ListDeserializator {
  static List<T> deserialize<T>(dynamic source, Serializer<T> serializer) {
    return source == null 
      ? null
      : (source as List)
        .map((i) => standardSerializers.deserializeWith(serializer, i))
        .toList();
  }
}

